export interface Information {
    data : Array<[]>;
    success : boolean;
    error:"";
}

export interface CountedInfo{
    number: number;
    quantity: number;
    firstPosition : Number;
    lastPosition : Number;
}

export interface Paragraph {
    data: Array<any>;
    hasCopyright: boolean;
    number:number;
    paragraph : string

}

export interface ICountedParagraph{
    index: number;
    paragraph: string;
    letterCount: IDictionaryCount;
    numericalSum: number;
}

export interface IDictionaryCount{
    numA: number;
    numB: number;
    numC: number;
    numD: number;
    numE: number;
    numF: number;
    numG: number;
    numH: number;
    numI: number;
    numJ: number;
    numK: number;
    numM: number;
    numN: number;
    numL: number;
    numO: number;
    numP: number;
    numQ: number;
    numR: number;
    numS: number;
    numT: number;
    numU: number;
    numV: number;
    numW: number;
    numX: number;
    numY: number;
    numZ: number;
}
