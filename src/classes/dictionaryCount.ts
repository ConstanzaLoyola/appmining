import { IDictionaryCount } from "src/interfaces";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class DictionaryCount implements IDictionaryCount{
    public numA: number;
    public numB: number;
    public numC: number;
    public numD: number;
    public numE: number;
    public numF: number;
    public numG: number;
    public numH: number;
    public numI: number;
    public numJ: number;
    public numK: number;
    public numM: number;
    public numN: number;
    public numL: number;
    public numO: number;
    public numP: number;
    public numQ: number;
    public numR: number;
    public numS: number;
    public numT: number;
    public numU: number;
    public numV: number;
    public numW: number;
    public numX: number;
    public numY: number;
    public numZ: number;
}