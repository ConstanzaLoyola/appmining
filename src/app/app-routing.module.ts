import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ListNumberComponent } from './components/list-number/list-number.component';
import { ParagraphComponent } from './components/paragraph/paragraph.component';

// se determinan las rutas de cada una de las components
const routes: Routes = [
  {path:"home",component:HomeComponent},
  {path:"listNumber", component:ListNumberComponent},
  {path:"paragraph", component:ParagraphComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'home' } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
