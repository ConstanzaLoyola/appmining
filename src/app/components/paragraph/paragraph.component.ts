import { Component, OnInit } from "@angular/core";
import { MiningtagService } from "src/app/services/miningtag.service";
import { Paragraph, ICountedParagraph, IDictionaryCount } from "src/interfaces";
import { DictionaryCount } from "src/classes/dictionaryCount";
import { count } from "console";
import { element } from "protractor";
import { NumberFormatStyle } from "@angular/common";
import { cpuUsage } from "process";
import { ElementSchemaRegistry } from "@angular/compiler";

@Component({
  selector: "app-paragraph",
  templateUrl: "./paragraph.component.html",
  styleUrls: ["./paragraph.component.css"],
})
export class ParagraphComponent implements OnInit {
  paragraph: Paragraph[];
  loadParagraph: boolean;
  countedText: ICountedParagraph[];

  constructor(private _srvMining: MiningtagService) {}

  ngOnInit() {
    this.loadParagraph = false;
    this.getParagraph();
  }

  getParagraph(): void {
    this._srvMining.getParagraphs().subscribe((res) => {
      if (res.success == true) {
        this.paragraph = JSON.parse(res.data);
      } else {
        return "ocurrio un error al obtener la data";
      }

      this.countedText = this.getNumberOfCharacters(this.paragraph);

      this.loadParagraph = true;
    });
  }

  // Se obtiene cantidad de letras repetidas dentro de un parrafo
  // Ej : Hello World => H:1 - E:1 - L:3 - O:2 - W:1 - R:1 - D:1
  getNumberOfCharacters(paragraphs: Paragraph[]): ICountedParagraph[] {
    //se recibe parametro de tipo Paragraph[] y retorna la estructura de la interfaz ICountedParagraph[]
    let result = Array<ICountedParagraph>();
    let self = this; // se declara esta variable para poder utilizar el metodo numberSum

    //Si paragraphs tiene datos y paragraphs.length es mayor a 0
    //recorre el abecedario completo, en la cual se utiliza match para determinar si el parrafo contiene la letra que se esta recorriendo.
    if (paragraphs && paragraphs.length > 0) {
      paragraphs.forEach(function (paragraphValue, paragraphIndex) {
        let count: IDictionaryCount = new DictionaryCount();
        count.numA = (
          paragraphValue.paragraph.toLowerCase().match(/a/g) || []
        ).length;
        count.numB = (
          paragraphValue.paragraph.toLowerCase().match(/b/g) || []
        ).length;
        count.numC = (
          paragraphValue.paragraph.toLowerCase().match(/c/g) || []
        ).length;
        count.numD = (
          paragraphValue.paragraph.toLowerCase().match(/d/g) || []
        ).length;
        count.numE = (
          paragraphValue.paragraph.toLowerCase().match(/e/g) || []
        ).length;
        count.numF = (
          paragraphValue.paragraph.toLowerCase().match(/f/g) || []
        ).length;
        count.numG = (
          paragraphValue.paragraph.toLowerCase().match(/g/g) || []
        ).length;
        count.numH = (
          paragraphValue.paragraph.toLowerCase().match(/h/g) || []
        ).length;
        count.numI = (
          paragraphValue.paragraph.toLowerCase().match(/i/g) || []
        ).length;
        count.numJ = (
          paragraphValue.paragraph.toLowerCase().match(/j/g) || []
        ).length;
        count.numK = (
          paragraphValue.paragraph.toLowerCase().match(/k/g) || []
        ).length;
        count.numM = (
          paragraphValue.paragraph.toLowerCase().match(/m/g) || []
        ).length;
        count.numN = (
          paragraphValue.paragraph.toLowerCase().match(/n/g) || []
        ).length;
        count.numL = (
          paragraphValue.paragraph.toLowerCase().match(/l/g) || []
        ).length;
        count.numO = (
          paragraphValue.paragraph.toLowerCase().match(/o/g) || []
        ).length;
        count.numP = (
          paragraphValue.paragraph.toLowerCase().match(/p/g) || []
        ).length;
        count.numQ = (
          paragraphValue.paragraph.toLowerCase().match(/q/g) || []
        ).length;
        count.numR = (
          paragraphValue.paragraph.toLowerCase().match(/r/g) || []
        ).length;
        count.numS = (
          paragraphValue.paragraph.toLowerCase().match(/s/g) || []
        ).length;
        count.numT = (
          paragraphValue.paragraph.toLowerCase().match(/t/g) || []
        ).length;
        count.numU = (
          paragraphValue.paragraph.toLowerCase().match(/u/g) || []
        ).length;
        count.numV = (
          paragraphValue.paragraph.toLowerCase().match(/v/g) || []
        ).length;
        count.numW = (
          paragraphValue.paragraph.toLowerCase().match(/w/g) || []
        ).length;
        count.numX = (
          paragraphValue.paragraph.toLowerCase().match(/x/g) || []
        ).length;
        count.numY = (
          paragraphValue.paragraph.toLowerCase().match(/y/g) || []
        ).length;
        count.numZ = (
          paragraphValue.paragraph.toLowerCase().match(/z/g) || []
        ).length;

        
        result.push({
          index: paragraphIndex,
          paragraph: paragraphValue.paragraph,
          letterCount: count,
          numericalSum: self.numberSum(paragraphValue.paragraph),//se obtiene suma de los numeros que aparecen dentro del parrafo

        });
      });
    }

    return result;
  }

  //Se obtiene suma de los numeros que aparecen dentro del parrafo
  numberSum(paragraph: string): number {
    let sum = 0;
    let testCondition = /\d/g;
    if (testCondition.test(paragraph)) {
      //revisa si hay numeros
      let num = paragraph.replace(/[^0-9]/g, "|").split("|"); //remplaza las letras por | y luego los quita para dejar solo los numeros
      console.log("num", num);
      //se recorre la variable si es mayor a 0
      if (num.length > 0) {
        num.forEach((element) => {
          // por cada pasada y si esta es distinto a vacio se suman los elementos
          if (element != "") {
            sum += parseInt(element);
          }
        });
      }
    }

    return sum;
  }
}
