import { Component, OnInit } from "@angular/core";
import { MiningtagService } from "src/app/services/miningtag.service";
import { Information, CountedInfo } from "src/interfaces";

@Component({
  selector: "app-list-number",
  templateUrl: "./list-number.component.html",
  styleUrls: ["./list-number.component.css"],
})
export class ListNumberComponent implements OnInit {
  //se declaran variables a utilizar
  loadNumber: boolean;
  listNum: Information;
  duplicated: any;
  quantity: CountedInfo[];

  //se inicializa el servicio
  constructor(private _srvMining: MiningtagService) {}

  //se llama al metodo para obtener todos los números y se deja el preloader en false
  ngOnInit() {
    this.loadNumber = false;
    this.getListNumber();
  }

  //Obtiene Lista Numeros Aleatorios
  getListNumber(): void {
    this._srvMining.getRandomNumbers().subscribe((res: Information) => {
      //Se llama al servicio

      //se valida que succes sea true
      if (res.success == true) {
        this.listNum = res;

        this.duplicated = this.listNum.data;

        this.quantity = this.getQuantity(this.duplicated);

        this.loadNumber = true; //cuando ya se ha cargado la información se cambia preloader a true
      } else {
        alert("Ha ocurrido un error");
      }
    });
  }

  //Se obtiene la cantidad de ocurrencias de un mismo número y su posición
  getQuantity(initialQuantity: number[]): CountedInfo[] {
    let originalSort = [...initialQuantity]; // se obtiene una copia de la variable y sus propiedades
    let newSort = initialQuantity.sort((prev, next) => prev - next); // realiza el orden de menor a mayor

    let counted = Array<CountedInfo>();
    let numLoop = null;
    let counLoop = 0;
    let idxFirst = 0;
    let idxLast = 0;

    //recorre los numeros y la cantidad de estos
    for (let i = 0; i < newSort.length; i++) {
      if (newSort[i] != numLoop) {
        if (counLoop > 0) {
          counted.push({
            number: numLoop,
            quantity: counLoop,
            firstPosition: idxFirst,
            lastPosition: idxLast,
          });
        }

        numLoop = newSort[i];
        counLoop = 1;
        //determina la primera posicion y ultima posicion del numero
        //Ej: [2,4,6,4,7,4] => La posicion del numero 4 ; first : 1  | last:5
        idxFirst = originalSort.indexOf(newSort[i]) + 1;
        idxLast = originalSort.lastIndexOf(newSort[i]) + 1;
      } else {
        counLoop++;
      }
    }

    if (counLoop > 0) {
      counted.push({
        number: numLoop,
        quantity: counLoop,
        firstPosition: idxFirst,
        lastPosition: idxLast,
      });
    }
    return counted;
  }
}
