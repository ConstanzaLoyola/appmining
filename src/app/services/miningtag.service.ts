import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Information } from "src/interfaces";

@Injectable({
  providedIn: "root",
})
export class MiningtagService {
  miningAPI: any;

  //Se inicializa el servidor HttpClient para poder realizar las peticiones
  constructor(private _http: HttpClient) {
    //se obtiene URL de la API
    this.miningAPI = environment.urlAPI;
  }

  //Se obtiene desde la API los números Aleatorios con la estrucutra de Information
  getRandomNumbers(): Observable<Information> {
    return this._http.get<any>(`${this.miningAPI}/array.php`);
  }

  //Se obtienen parrafos
  getParagraphs(): Observable<any> {
    return this._http.get<any>(`${this.miningAPI}/dict.php`);
  }
}
